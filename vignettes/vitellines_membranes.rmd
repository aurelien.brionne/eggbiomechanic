---
title: "vitellines membranes mechanical properties from simulated data"
author:
- name: Aurelien Brionne
  affiliation: Institut National de la Recherche Agronomique (INRA)
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  BiocStyle::html_document:
    highlight: tango
vignette: >
  %\VignetteIndexEntry{vitellines_membranes}
  %\VignetteEngine{knitr::rmarkdown}
  %\usepackage[utf8]{inputenc}
---

```{r setup,include=FALSE,cache=F}
###################
# knitr document options
knitr::opts_chunk$set(
    message=FALSE,comment=NA,warning=FALSE,fig.height = 10
)
```

# Introduction

In this vignette we illustrate vitellines membranes mechanical properties from simulated data.

# Material and Methods

## Methods

Measurements of vitellines membranes breaking strength (Sd) and elasticity were carried out using an [Instron](http://www.instron.tm.fr/wa/home/default_en.aspx) (Instron type 5543, UK527) . The Sd corresponds to the Breacking strength (in N) applied to the center of the egg yolk, and the elasticity to traveled distance.

## Statistics

A statistical analysis for unpaired samples was followed according the diagram (see above).

```{r diagram,fig.height=4,echo=FALSE}
###################
# statistical diagram
eggbiomechanic::stat_diagram()
```

# Results

## Instron raw data processsing

The raw data from Instron were treated using the `eggbiomechanic::Instron_mbvit` function.

We also exclude at this step the samples 29 and 44 from C (C_29, C_44) that exhibit atypical behavior and will be excluded from the analysis.

```{r Instron}
# raw Instron data processing
vitmb<-eggbiomechanic::Instron_vitmb(
    system.file("extdata/mbvit",package = "eggbiomechanic"),
    minFmm=1,
    remove_samples=c("C_29","C_44")
)
```

An object summary is available by calling the object.

```{r Instron_summary}
# display summary
vitmb
```

All data values could be extract from the `vitmb` object using the `eggbiomechanic::get_table` method with the named table to extract which could be *raw_data*, *Breaking_strength*, *treated_data*, *AUC*, or *summary* in this case.

For example, we extract `vitmb` object <u>summary table</u> with:

```{r get_table}
# get the summary table
Data=eggbiomechanic::get_table(vitmb,"summary")
```

Then, the obtained table could be printed with `utils::write.table` function,

```{r print_table,eval=FALSE}
# write the table
fwrite(Data,file="./data/output/summary.txt",sep="\t")
```

or displayed it in interactive mode with: 

```{r show_table}
# display the table
eggbiomechanic::display_table(Data)
```

## Compression curves plots

### raw compression curves

We can display the raw compression curves.

```{r plot_D0,fig.height=10}
# plot individual trace according class
eggbiomechanic::plots(vitmb,"A")
```

### compression curves (cutted) overlay

We can display the cutted compression curves overlay.

```{r cutted_curves_plot,fig.height=8}
# plot individual trace according class
eggbiomechanic::plots(vitmb)
```

## Statistics

### Breaking Strength

```{r Breaking_strength}
# statistical test
Breaking_strength<-eggbiomechanic::var_test(vitmb,"breaking_strength~class")
```

### Elasticity

```{r Elasticity}
# statistical test
elasticity<-eggbiomechanic::var_test(vitmb,"elasticity~class")
```

### Area under the curve

```{r AUC}
# statistical test
AUC<-eggbiomechanic::var_test(vitmb,"AUC~class")
```

<u>NB:</u> All statistical results are available by calling the object (which could be in this case: Breaking_strength, elasticity, or AUC).
