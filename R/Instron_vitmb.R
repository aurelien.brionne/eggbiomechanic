#' @title  Determine egg vitellines membranes compression curve mechanical properties.
#' @description This function is used to determine the biomechanical properties from vitellines membranes compression
#' curves obtained with the Instron Materials Testing Machine (type 5543).
#' @importFrom data.table data.table := rbindlist
#' @importFrom ggplot2 ggplot aes geom_text geom_line position_stack theme_bw facet_wrap theme
#' @importFrom zoo rollmean
#' @family vitellines_membranes
#' @param input the Instron raw data path (.raw files).
#' @param minFmm The minimum displacement value (default to 1 mm) threshold before computing the breacking strength.
#' @param  remove_samples named samples to remove as sample or filename_sample (if use_file_name=T).
#' @param use_file_name add file name to sample (default to T).
#' @details This function do for each sample:
#' \enumerate{
#'  \item read and parse the Instron raw data with \code{\link{Instron_load}}.
#'  \item extract the Breacking strength position (N~mm).
#'  \item remove all values after the  Breacking strength and compute Area Under Curve (AUC) using the trapezoidal method.
#'  \item draw compression curves.
#'}
#' @return a \code{\link{vitellines_membranes-class}} object.
#' @include vitellines_membranes.R
#' @examples
#' ###################
#' # read and parse the Instron raw data files from a target directorie
#' vitmb<-Instron_vitmb(
#'  input=system.file("extdata/mbvit",package = "eggbiomechanic"),
#'  minFmm=1,
#'  remove_samples=c("D14_29","D14_44")
#' )
#' @export
Instron_vitmb=function(input,minFmm=1,remove_samples=NULL,use_file_name=TRUE){

    # parse files
    if(input=="") stop("please enter a valid path to Instron .raw folder")

    # parse files
    Data<-eggbiomechanic::Instron_load(input,use_file_name)

    # remove samples
    if(!is.null(remove_samples)){

        # remove samples
        Data<-Data[!samples%in%remove_samples]
    }

    ## Breaking_strength

    # extract breacking strenth by samples
    Breaking_strength=lapply(unique(Data$samples),function(x){

        # extract breacking strenth by samples
        Data[x,on="samples"][mm>minFmm & N==max(N)]
    })
    Breaking_strength<-rbindlist(Breaking_strength)

    ## cutted data

    # cut curves according Breaking_strength
    Datacut<-lapply(unique(Data$samples),function(x){

        # Breaking_strength location
        pos<-unlist(Breaking_strength[x,"mm",on="samples",with=FALSE])

        # cut Data
        Data[samples==x & mm<=pos]
    })
    Datacut<-rbindlist(Datacut)

    ## AUC

    # compute AUC (trapezium estimation)
    AUC=Datacut[,
        {
            x=mm;
            id=order(x);
            y=N;
            AUC= sum(diff(x[id])*rollmean(y[id],2));
            list(AUC=AUC)
        },
        by=c("samples","class","sample")
    ]

    ## build values summary

    # build summary function
    fun=function(data){

        # build a table
        Data=data.table(t(as.vector(summary(data))))
        names(Data)<-c("min","q1","median","mean","q3","max")

        # add some columns
        Data[,
             `:=`(
                n=length(data),
                IQR=q3-q1,
            sd=sd(data)
            )
        ]

        # return
        return(Data)
      }

    # compute summary
    values<-lapply(unique(Data$class),function(x){

        # bind
        rbind(

            # compute summary for elasticity (mm)
            data.table(
                type="elasticity",
                 class=x,
                fun(data=Breaking_strength[class==x,mm])
            ),

            # compute summary for breaking strength (N, Sd)
            data.table(
                type="F",
                class=x,
                fun(data=Breaking_strength[class==x,N])
                ),

            # compute summary for AUC
            data.table(
                type="AUC",
                class=x,
                fun(data=AUC[class==x,AUC])
            )
        )
      })

    # bind results
    values=rbindlist(values)

    # bind results
    setorderv(values,c("type","class"))

    # round digits
    values<-values[,
        lapply(.SD,function(x){round(x,digits=2)}),
        .SDcols=3:ncol(values),
        by=c("type","class")
    ]

    ## individual plot

    # individual plot by class
    p<-lapply(unique(Data$class),function(x){
        ggplot(Data[class==x],aes(mm,N,group=samples)) +
        geom_line(col="royalblue") +
        theme_bw() +
        geom_text(
            data=Breaking_strength[class==x],
            size=2,
            colour="magenta",
            position = position_stack(vjust = 1.2),
            aes(label =round(N,digits=2))
        )
    })
    names(p)<-unique(Data$class)

    ## class plot on cutted values

    # plot individual trace according class and time
    p1<-ggplot(Datacut,aes(mm,N,group=samples,colour=class)) +
    geom_line() + 
    theme_bw()

    # plot individual trace according class and time
    p2<-p1 +
    facet_wrap(~class,ncol=1) +
    theme(legend.position="none")

    ## return results in list object

    # results object
    methods::new(
        "vitellines_membranes",
        parameters=list(
            input=input,
            minFmm=minFmm,
            removed_samples=remove_samples
        ),
        tables=list(
            raw_data=Data,
            Breaking_strength= Breaking_strength,
            treated_data=Datacut,
            AUC=AUC,
            summary=values
        ),
        plots=list(
            plot_samples=p,
            plot_samples_class=p1,
            plot_samples_class_overlay=p2
        )
    )
}
