#' @title eggshell class object definition
#' @description This class is invoked by \code{\link{Instron_eggshell}} method to store results.
#' @slot parameters the input parameters in \code{list} with slope_start_range, slope, and threshold elements.
#' @slot tables tables in \code{list} with raw_data, results, and summary elements.
#' @slot plots samples compression curves (ggplot list objects).
setClass(
    "eggshell",
    slots=c(
        parameters="list",
        tables="list",
        plots="list"
    )
)

#' @aliases eggshell
setMethod(
    "show",
    signature="eggshell",
    function(object){
        
        # cat some text
        cat("eggshell",
            "-Parameters:",
            paste("  input path:",slot(object,"parameters")$input),
            paste("  slope_start_range:",paste(slot(object,"parameters")$slope_start_range,collapse="-")),
            paste("  slope:",slot(object,"parameters")$slope),
            paste("  broken egg threshold:",slot(object,"parameters")$threshold),
            paste("  samples removed:",paste(slot(object,"parameters")$removed_samples,collapse=", ")),
            "- Available tables: raw_data, results, summary",
            "- Values summary by type and class: ",sep="\n"
        )
        
        # extract summary
        Data<-slot(object,"tables")$summary
        
        # display values summary
        print(
            Data,
            nrows=nrow(Data)
        )
    }
)