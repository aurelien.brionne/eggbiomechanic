#' @title  Determine eggshell compression curve mechanical properties.
#' @description This function is used to determine the mechanical properties from eggshell compression curves
#' obtained with the Instron Materials Testing Machine (type 5543).
#' @importFrom data.table data.table := rbindlist
#' @importFrom ggplot2 ggplot aes geom_text geom_line geom_abline geom_hline
#' @param input the Instron data path to .raw files.
#' @param slope_param a list of slope parameters
#' \describe{
#'  \item{global}{ a list of global slope paprameters
#'   \describe{
#'    \item{slope_start_range}{
#'     \code{vector} with the min and max displacement value, in mm, where the linear regression should begin according and second numerical derivative start point selection. (default to NULL)
#'    }
#'    \item{slope}{
#'     This argument is use in order to determine the \emph{static stifness} (= the slope) of a part of the compression
#'     curve using a linear regression. The value(s) could be:
#'     \itemize{
#'      \item a numeric value: which correspond to the maxima strength (10N by default) point of the curve to use.
#'      This value is used in combination with the \code{slope_start_range} parameter.
#'      \item a \code{\link[base]{vector}} range: with the min and max distance (in mm) of the curve to use.
#'      \item the "F" \code{\link[base]{character}}, where the breacking strength value of the curve is used
#'      in combination with the \code{slope_start_range} parameter.
#'     }
#'    }
#'   }
#'  }
#'  \item{except}{a list of specific slope parameters (see above) with samples identifiant in names}
#' }
#' @param threshold The breacking strength (in Newton)  threshold for sort broken and unbroken eggs.
#' @param remove_samples named samples to remove as sample or filename_sample (if you used use_file_name=TRUE).
#' @param use_file_name add file name to sample (default to TRUE).
#' @details This function read and parse the Instron raw data with \code{\link{Instron_load}}, return a \code{\link[data.table]{data.table}} with
#' for each sample curve the \emph{breacking strength} (F), the status of the eggshell (broken or unbroken according the \code{threshold} parameter),
#' the \emph{static stifness} (Sd, the slope) with the corresponding pearson correlation of the linear regression.\cr
#' This function also draw all compression curves and add the computed values on the graphs.\cr
#' @return a \code{\link{eggshell-class}} object.
#' @include eggshell.R
#' @examples
#' ###################
#' # read and parse the Instron raw data files from a target directorie
#' eggshell<-eggbiomechanic::Instron_eggshell(
#'     input=system.file("extdata/eggshell",package = "eggbiomechanic"),
#'     slope_param=list(
#'         global=list(
#'             slope_start_range = c(0, 0.2),
#'             slope = 10
#'         )
#'     ),
#'     threshold = 20,
#'     remove_samples=NULL,
#'     use_file_name = FALSE
#' )
#' @export
Instron_eggshell<-function(input,slope_param=list(global=list(slope_start_range=c(0,0.1),slope=10),except=NULL),
threshold=20,remove_samples=NULL,use_file_name=TRUE){

    # parse files
    Data<-eggbiomechanic::Instron_load(input,use_file_name)

    # check exception samples names
    if(!is.null(slope_param$except) && !all(names(slope_param$except)%in%Data$samples)){
        stop("at least one samples exception do not exist")
    }

    # remove samples
    if(!is.null(remove_samples)){

        # remove samples
        Data<-Data[!samples%in%remove_samples]
    }

    ## breacking strenth

    # extract breacking strenth by samples
    Breaking_strength=Data[,{
      pos=which.max(N);
      list(
        mm=mm[pos]+0.1,
        N=N[pos],
        text=round(N[pos],digits=1))
    },by="samples"]

    ## broken eggs

    # locate broken eggs
    broken_egg<-data.table( Breaking_strength[,c("samples","N"),with=FALSE])
    broken_egg[,"text":=""]
    broken_egg[N<threshold,`:=`(N=25,mm=0.45,text="broken egg")]

    ## slope

    # numeric derivation function
    deriv=function(Data){

        # rename
        names(Data)<-c("id","x","y")

        # pos
        pos<-2:(nrow(Data)-1)

        # derivate
        values=vapply(pos,function(i){
            (Data$y[i+1]-Data$y[i-1])/(Data$x[i+1]-Data$x[i]-1)
        },0)

        # return
        data.table(
            id=unique(Data$id),
            x=Data$x[pos],
            y=values
         )
    }

    # define egg slope
    egg_slope=lapply(unique(Data$samples),function(x){
      
        # globale start range parameters
        slope_start_range<-slope_param$global$slope_start_range
        
        # globale slope parameters
        slope=slope_param$global$slope

        # exception slope parameters
        if(!is.null(slope_param$except) && x%in%names(slope_param$except)){
            
            # start range excpetion
            if("slope_start_range"%in%names(slope_param$except[[x]])){
                
                # globale start range parameters
                slope_start_range<-slope_param$except[[x]][["slope_start_range"]]
            }

            # slope exception
            if("slope"%in%names(slope_param$except[[x]])){
                
                # globale start range parameters
                slope<-slope_param$except[[x]][["slope"]]
            }
        }

        # subset Data
        Data<-Data[x,on="samples"]

        # subset the left part of the graph
        Data<-Data[1:which.max(N)]

        # select
        Data<-Data[,c("samples","mm","N"),with=FALSE]

        # first derivate
        first=deriv(Data)

        # second derivate
        second=deriv(first)

        # start
        if(is.null(slope_start_range)){
        
            # start without slope range selection
            start=second[y==max(y),"x",with=FALSE]

        }else{
          
            # start with slope range selection
            start=second[x<slope_start_range[2] & x>slope_start_range[1]][y==max(y),"x",with=FALSE]
        }

        # slope stop
        if(!is.numeric(slope)){

            # max slope
            end=Data[max(N)==N,"mm",with=FALSE]

        }else{

            # slope stop
            if(is.numeric(slope) && length(slope)>1){

                # user defined range
                end<-tail(slope,n=1)

            }else{

                # end
                if(is.null(slope_start_range)){

                    # end without range
                    end<-tail(Data[N<=slope,m],n=1)

                }else{

                    # end range
                    end<-tail(Data[N<=slope & mm<slope_start_range[2],mm],n=1)
                }
            }
        }

        # define Dat
        Dat<-Data[mm>=unlist(start) & mm<=unlist(end)]

        # for not empty Dat
        if(nrow(Dat)>0){

            # extract slope
            reg<-coef(lm(Dat$N~Dat$mm))

            # extract pearson correlation
            cor=round(cor(Dat$N,Dat$mm),digits=3)

        }else{

            # return NA values
            reg=rep(NA,2)
            cor=NA
        }

        # return data.table
        data.table(
            samples=x,
            intercept=reg[1],
            slope=reg[2],
            pearson_cor=cor
        )
    })
    egg_slope=rbindlist(egg_slope)

    # define egg slope_text
    egg_slope_text=data.table(
        samples=egg_slope$samples,
        mm=Breaking_strength$mm-0.15,
        N=5,
        text=round(egg_slope$slope,digits = 1)
    )

    ## results

    # merge the results
    results=data.table(
        unique(Data[,c("class","sample"),with=FALSE]),
        Breaking_strength[,c("samples","mm","N"),with=FALSE],
        egg_slope[,c("slope","pearson_cor"),with=FALSE],
        broken_egg=broken_egg$text
    )

    # rename slope an d N as Sd and F
    names(results)[c(5,6)]<-c("F","Sd")

    # remove values from broken eggs
    results[
        "broken egg",
        `:=`(
            mm=NA,
            F=NA,
            Sd=NA,
            pearson_cor=NA
        ),
        on="broken_egg"
    ]


    ## build values summary

    # build summary function
    fun=function(data){

        # remove NA
        data=na.omit(data)

        # build a table
        Data=data.table(t(as.vector(summary(data))))
        names(Data)<-c("min","q1","median","mean","q3","max")

        # add some columns
        Data[,
             `:=`(
                n=length(data),
                IQR=q3-q1,
                sd=sd(data)
            )
        ]

      # return
      return(Data)
    }

    # compute summary
    values<-lapply(unique(results$class),function(x){

        # bind
         rbind(

            # compute summary for breaking strength
            data.table(
                type="F",
                class=x,
                fun(data=results[class==x,F])
            ),

            # compute summary for Sd
            data.table(
                type="Sd",
                class=x,
                fun(data=results[class==x,Sd])
            )
        )
    })

    # bind results
    values=rbindlist(values)

    # sort results
    setorderv(values,c("type","class"))

    # round digits
    values<-values[,
        lapply(.SD,function(x){round(x,digits=2)}),
        .SDcols=3:ncol(values),
        by=c("type","class")
    ]

    ## plot curves and calulated values
    
    # Data plot
    p<-ggplot(Data,aes(mm,N,group=samples)) +

    # blue line graph
    geom_line(col="blue") +

    # add breaking strength
    geom_text(
        data=Breaking_strength,
        size=3,
        colour="blue",
        aes(label =text)
    ) +

    # add slope
    geom_abline(
        data=egg_slope,
        aes(slope=slope,intercept=intercept),
        colour="darkgreen"
    ) +

    # add slope value
    geom_text(
        data=egg_slope_text,
        size=3,
        colour="darkgreen",
        aes(label =text)
    )+

    # add broken egg line
    geom_hline(
        yintercept=threshold,
        colour="magenta",
        lty=2
    )+

    # add broken
    geom_text(
        data=broken_egg,
        na.rm=TRUE,
        size=2,
        colour="magenta",
        aes(label =text)
    )

    ## return results

    # results object
    new(
        "eggshell",
      parameters=list(
        input=input,
        slope_param=slope_param,
        threshold=threshold
      ),
      tables=list(
        raw_data=Data,
        results=results,
        summary=values
      ),
      plots=list(plot_samples=p)
    )
}
