#' @title  Perform the statistical test of a given variable.
#' @description Perform Linear models or  linear mixed-effects models statistics a given variable using \code{\link{stat_lm}} or \code{\link{stat_lme}} chains, respectively.
#' @importFrom data.table .SD .N := data.table setorderv
#' @import ggplot2
#' @param data \code{\link{vitellines_membranes-class}}, or \code{\link{eggshell-class}} objects, or any \code{\link[data.table]{data.table}}.
#' @param f The \code{\link[stats]{formula}}.
#' @param family The \code{\link[stats]{glm}}.
#' @param random The random effect from \code{\link[nlme]{lme}}.
#' @param method The method to use from \code{\link[nlme]{lme}}.
#' @param lim a limits \code{\link[base]{vector}} range values to use for graph display.
#' @param outliers outliers samples to remove.
#' @param figs output folder to print graphs (default to NULL).
#' @param prefix additionnal graph prefix.
#' @param threshold multcompLetters threshold (default to 0.05)
#' @param print print screen (default to TRUE)
#' @details
#' This function perform statistical tests according \code{\link{stat_lm}}, \code{\link{stat_glm}} or \code{\link{stat_lme}} chains and return results as text and also graphics.
#' For \code{\link{vitellines_membranes-class}}, the available variable to test are: "elasticity","breacking_strength", or "AUC".\cr
#' For \code{\link{eggshell-class}}, the available variable to test are: "elasticity" or "breacking_strength".\cr
#' @return a \code{\link[data.table]{data.table}} and text output.
#' @include eggshell.R vitellines_membranes.R
#' @examples
#' \dontrun{
#' # test vitellines membranes breacking_strength
#' var_test(vitmb,"N")
#' }
#' @name var_test
#' @rdname var_test-methods
#' @exportMethod var_test
setGeneric(
    name="var_test",
    def=function(data,f,family=NULL,random=NULL,method="ML",threshold=0.05,lim=NULL,outliers=NULL,figs=NULL,prefix=NULL,print=TRUE){
        standardGeneric("var_test")
    }
)

#' @rdname var_test-methods
#' @aliases var_test
setMethod(
    "var_test",
    signature="ANY",
    definition=function(data,f,family,random,method,threshold,lim,outliers,figs,prefix,print){

        # extract Data
        Data=data

        # copy variable in var object
        variable=sub("~.+$","",f)
        var=variable

        # extract Data from vitellines_membranes or eggshell class objects
        if(is(Data,"vitellines_membranes") | is(Data,"eggshell") | is(Data,"eggshell_biomechanic")){

            # for vitellines_membranes class object
            if(is(Data,"vitellines_membranes")){

                # check arguments
                variable<-match.arg(variable,c("elasticity","breaking_strength","AUC"))

                # breaking strength
                if(variable%in%c("elasticity","breaking_strength")){

                    # extract Data
                    Data<-slot(Data,"tables")[["Breaking_strength"]]

                    # rename elasticity variable
                    if(variable=="elasticity"){

                        # rename variable
                        var="mm"

                        # rename f
                        f<-gsub("elasticity","mm",f)
                    }

                    # rename breaking_strength variable
                    if(variable=="breaking_strength"){

                        # rename variable
                        var="N"

                        # rename f
                        f<-gsub("breaking_strength","N",f)
                    }
                }

                # AUC
                if(var=="AUC"){Data<-slot(Data,"tables")[["AUC"]]}

            }else{

                # for eggshell_biomechanic class object
                if(is(Data,"eggshell_biomechanic")){Data<-slot(Data,"tables")[["results"]]}
            }
        }

        # extract Data
        if(is.null(random)){
            Data<-Data[,unique(unlist(strsplit(f,"\\~|\\+|\\:|\\*"))),with=FALSE]

        }else{
            Data<-Data[,unique(c(unlist(strsplit(f,"\\~|\\+|\\:|\\*")),unlist(strsplit(random,"\\+|\\/|\\|"))[-1])),with=FALSE]
        }

        # remove outliers
        if(!is.null(outliers)){

            # remove it
            Data<-Data[-c(outliers),]
        }
 
        # modify header
        names(Data)[1]<-"value"

        # convert to factor
        Dat<-data.table(
            value=Data[,value],
             Data[,
                lapply(.SD,function(x){factor(x,levels=unique(x))}),
                .SDcols=2:ncol(Data)
            ]
        )
 
        # check table
        check<-Dat[
            !is.na(value),
            .N,
            by=c(strsplit(f,"\\~|\\+|\\*|:")[[1]][-1])
        ]

        # enough observations
        if(nrow(check)>1 && all(check$N>=1)){
  
            if(is.null(random)){

                if(is.null(family)){

                    # statistical test
                    Stats<-eggbiomechanic::stat_lm(Dat,f,threshold=threshold,print=print)

                }else{

                    # statistical test
                    Stats<-eggbiomechanic::stat_glm(Dat,f,family,threshold=threshold,print=print)
                }
            }else{

                # statistical test
                Stats<-eggbiomechanic::stat_lme(Dat,f,random,method,threshold=threshold,print=print)
            }
  
            # summary for tested class
            Class<-names(Stats$contrasts_test)

            # tested class
            Summary<-lapply(Class,function(x){

                # tested class
                x<-gsub(" \\(.+$","",x)

                # if interaction
                if(length(grep(":|\\*|\\+",x))==1){

                    # remove group
                    if("group"%in%names(Data)){Data[,group:=NULL]}

                    # add a group column for interaction
                    Data[,
                        "group":=paste(.SD,collapse=","),
                        .SDcols=strsplit(x,":|\\*|\\+")[[1]],
                        by=1:nrow(Data)
                    ]
                }else{

                    if(length(grep("\\|",x))==1){

                        # add a single group column
                        Data[,"group":=Data[,gsub("\\|.+$","",x),with=FALSE]]

                    }else{

                        # add a single group column
                        Data[,"group":=Data[,x,with=FALSE]]
                    }
                }

                # convert group to factor
                Data[,"group":=factor(group,levels=unique(group))]
    
                # return mean and median by class
                Data[,
                    list(
                        mean=round(mean(value,na.rm=TRUE),digits = 2),
                        median=round(median(value,na.rm=TRUE),digits = 2),
                        sd=round(sd(value,na.rm=TRUE),digits = 2),
                        min=round(min(value,na.rm=TRUE),digits = 2),
                        max=round(max(value,na.rm=TRUE),digits = 2),
                        q1=round(quantile(value,probs=0.25,na.rm =TRUE),digits = 2),
                        q3=round(quantile(value,probs=0.75,na.rm=TRUE),digits = 2)
                    ),
                    by=group
                ]
            })
            names(Summary)<-Class

            # print summary
            if(print==TRUE){print(Summary)}
  
            # graphs for tested class
            lapply(Class,function(x){
    
                # keep original name
                var=x

                # tested class
                x<-gsub(" \\(.+$","",x)
    
                # remove group
                if("group"%in%names(Data)){Data[,group:=NULL]}

                # if interaction
                if(length(grep(":|\\*|\\+",x))==1){

                    # add a group column for interaction
                    Data[,
                        "group":=paste(as.character(.SD),collapse=","),
                        .SDcols=strsplit(x,":|\\*|\\+")[[1]],
                        by=1:nrow(Data)
                    ]

                }else{

                    if(length(grep("\\|",x))==1){

                        # add a single group column
                        Data[,"group":=Data[,gsub("\\|.+$","",x),with=FALSE]]

                    }else{

                        # add a single group column
                        Data[,"group":=Data[,x,with=FALSE]]
                    }
                }

                # convert group to factor
                Data[,"group":=factor(group,levels=unique(group))]

                # density pot
                dp<-ggplot(Data,aes(value,fill=group,colour=group)) +
                geom_density(alpha=0.1) +
                labs(
                    x=var,
                    y=x,
                    title="density plot",
                    fill=var,
                    colour=var
                )+
                theme_bw()+
                theme(
                    panel.grid.major=element_blank(),
                    panel.grid.minor=element_blank()
                )

                # adjust lim if range provided
                if(!is.null(lim)){dp<-dp + scale_x_continuous(limits =lim)}

                # extract the last test results
                pvalues<-Stats[["contrasts_test"]][[var]][["letters"]]

                # extract the last test results
                pvalues<-merge(
                    pvalues,
                    Summary[[var]][,c("group","min","max")],with=FALSE,by="group"
                )

                # rename columns
                names(pvalues)[4]<-"value"

                # expend distance
                if(TRUE%in%(pvalues$value>3)){pvalues[,value:=value+1]}
                pvalues[,value:=value+value*0.1]

                # convert group to factor
                pvalues[,"group":=factor(group,levels=levels(Data$group))]

                # ordering pvalues
                setorderv(pvalues,"group")


                # init anova tag
                pval=Stats$anova_test[group==x,pval]

                # anova tag
                text<-rep("ns",length(pval))
                text[pval<0.1]<-"t"
                text[pval<0.05]<-"p<0.05"
                text[pval<0.01]<-"p<0.01"
                text[pval<0.001]<-"p<0.001"

                # add class
                text=paste(x,text,collapse="\n")

                # box plot
                bp<-ggplot(Data,aes(group,value,fill=group,colour=group)) +
                geom_boxplot(alpha=0.1) +
                geom_jitter(width = 0.1) +
                labs(
                    y=var,
                    title="box plot",
                    fill=var,
                    colour=var
                )+
                theme_bw()+
                theme(
                    panel.grid.major=element_blank(),
                    panel.grid.minor=element_blank(),
                    axis.text.x =element_text(angle = 90, hjust = 1)
                )+
                geom_text(
                    data=pvalues,
                    size=4,
                    aes(label=text)
                )+
                annotate(
                    "text",
                    x=0.5,
                    size=3,
                    y=(max(pvalues$value,na.rm=TRUE)+max(pvalues$value,na.rm=T)*0.03),
                    hjust = 0,
                    label=paste("anova:",text)
                )+
                scale_y_continuous(
                    limits =c(
                        min(pvalues$min),
                        (max(pvalues$value,na.rm=TRUE)+max(pvalues$value,na.rm=T)*0.05)
                    )
                )

                # adjust lim if range provided
                if(!is.null(lim)){bp<-bp + scale_y_continuous(limits =lim)}

                # extract the last test results
                stats<-Stats[["contrasts_test"]][[var]][["stats"]]

                # convert contrast to factors
                stats[,"contrast":=factor(contrast,levels=contrast)]

                # mean differences plots
                dm<-ggplot(stats,aes(contrast,diff))+
                geom_point(colour="blue")+
                geom_errorbar(
                    aes(
                        ymin=lwr,
                        ymax=upr
                    ),
                    width=.2,
                position=position_dodge(.9)
                )+
                theme_bw()+
                theme(
                    panel.grid.major=element_blank(),
                    panel.grid.minor=element_blank(),
                    axis.text.x =element_text(angle = 90, hjust = 1)
                )+
                labs(title="means differences")

                # remove special characters
                var=gsub("/","-",var)

                # print or not
                if(print==TRUE){

                    # return for html
                    grid.arrange(
                        Stats$plots$qqplot,
                        dp,
                        bp,
                        dm,
                        ncol=1
                    )
                }

                # return in pdf by gene
                if(!is.null(figs)){
                    pdf(paste(figs,prefix,paste(variable,var,sep="_"),".pdf",sep=""))
                        print(Stats$plots$qqplot)
                        print(dp)
                        print(bp)
                        print(dm)
                        dev.off()
                }
        })

            # return results in list
            list(
                data=class(data),
                variable=variable,
                Data=Data,
                summary=Summary,
                stats=Stats
            )
    }else{
        cat("\nnot enough observations for computation.\n")
    } 
    }
)
