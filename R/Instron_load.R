#' @title  read and parse raw data from Instron.
#' @description This internal function read and parse raw data from Instron.
#' @importFrom data.table data.table setorderv rbindlist :=
#' @param input the Instron raw data path provided by \code{\link{Instron_eggshell}} or \code{\link{Instron_vitmb}}.
#' @param use_file_name add file name to sample (default to TRUE).
#' @details This function is used by \code{\link{Instron_eggshell}} and \code{\link{Instron_vitmb}}
#' in order to read the Instron raw data files, from a target directory, and parse the results in a table with the columns:
#' \itemize{
#'  \item samples: concatenate values from class and sample column.
#'  \item class: the input file name.
#'  \item sample: the sample name.
#'  \item mm: the displacement value.
#'  \item N: the applied strength in Newton.
#' }
#' @return a \code{\link[data.table]{data.table}}.
#' @examples
#' ###################
#' # read and parse the Instron raw data files from a target directorie
#' Data<-Instron_load(input=system.file("extdata/eggshell",package = "eggbiomechanic"))
#' @keywords internal
#' @export
Instron_load=function(input,use_file_name=TRUE){

    if(length(grep("\\.raw",input))==1){

        files=input
        
    }else{
    
        # list files
        files=list.files(input,pattern="\\.raw$",full.names = TRUE)
    }

    # on each file
    Data<-lapply(files,function(x){

        # load data (windows encoding)
        Data=readLines(x,encoding ="latin1")

        # extract values
        Data=Data[substring(Data,1,1)%in%0:9|substring(Data,2,7)%in%"numero"]

        # remove \"
        Data<-gsub('\"',"",Data)

        # start of the test tube
        coord1<-which(substring(Data,1,1)=="n")

        # coord in data for line to skip
        val<-coord1[c(diff(coord1)==1,FALSE)]

        # remove empty egg
        if(length(val)>0){

            # remove egg without data in Data
             Data<-Data[-val]

            # retry start of the test tube
            coord1<-which(substring(Data,1,1)=="n")
        }

        # extract test tube numero
        Names<-gsub("numero;","",Data[coord1])

        # end of the test tube
        coord2<-c(coord1[-1],length(Data)+1)-1

        # positions of numeric data
        mat<-data.table(start=coord1+1,stop=coord2)

        # if error in data acquistion (press start but without egg)
        mat<-lapply(1:nrow(mat),function(x){

            # if fisrt values
            if(x==1){
                mat[x]
            }else{

                # dont select numero without data
                if(mat[x,stop]!=mat[x-1,stop]){mat[x]}
            }
        })

        # create a table
        mat<-rbindlist(mat)

        # extract data of test tub
        Data=lapply(1:nrow(mat),function(y){

            # subset Data
            Dat<-Data[mat[y,start]:mat[y,stop]]

            # extract deplacement and charge and convert in absolute data
            Dat=lapply(seq_along(Dat),function(z){

                # extract values
                values=gsub(",",".",strsplit(Dat[z],";")[[1]][2:3])

                # return values as data.table
                data.table(mm=values[1],N=values[2])
            })
            Dat<-rbindlist(Dat)

            # return values as data.table
            data.table(sample=Names[y],Dat)
        })
        Data<-rbindlist(Data)

        # convert Data in absolute numeric values
        Data[,
             `:=`(
                mm=abs(as.numeric(mm)),
                N=abs(as.numeric(N))
            )
        ]

        # scale deplacement on 0
        Data[,"mm":=mm-min(mm),by="sample"]

        # add file name to sample
        if(use_file_name==TRUE){

            # add file name to sample
            Data[,"class":=gsub("^.+/|\\.is_comp\\.raw|\\.raw","",x)]

            # add file name to sample
            Data[,"samples":=paste(class,sample,sep="_")]

        }else{

            # add file name to sample
            Data[,"class":=""]

            # add file name to sample
            Data[,"samples":=sample]
        }

        # return Data
        Data[,c("samples","class","sample","mm","N"),with=FALSE]
    })
    Data<-rbindlist(Data)

    # remove Data without sample id
    Data<-Data[sample!=""]

    # sort values by deplacement
    setorderv(Data,c("samples","mm"))
    
    #return
    return(Data)
}
