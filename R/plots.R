#' @title  plot compression curves
#' @description This function is used to display the compression curves from
#' \code{\link{vitellines_membranes-class}} or \code{\link{eggshell-class}} object.
#' @importFrom ggplot2 facet_wrap
#' @importFrom gridExtra grid.arrange
#' @param object a \code{\link{vitellines_membranes-class}} or \code{\link{eggshell-class}} object.
#' @param class a samples class of raw compression curves to display.
#' @details This method diplay the raw or treated compression curves of
#' \code{\link{vitellines_membranes-class}} or \code{\link{eggshell-class}} objects.
#' @return a \code{\link[ggplot2]{ggplot}} graph
#' @examples
#' \dontrun{
# # plot vitellines membranes D0 raw compression curves
#' eggbiomechanic::plot(vitmb,"D0")
#'
#' # plot vitellines membranes D07 raw compression curves
#' eggbiomechanic::plot(vitmb,"D7")
#' 
#' # plot vitellines membranes D14 raw compression curves
#' eggbiomechanic::plot(vitmb,"D4")
#'
#' # plot vitellines membranes treated compression curves and overlay
#' eggbiomechanic::plot(vitmb)
#'
#' # plot eggshell compression curves
#' eggbiomechanic::plots(eggshell)
#' }
#' @include eggshell.R
#' @include vitellines_membranes.R
#' @name plots
#' @rdname plots-methods
#' @exportMethod plots
setGeneric(
    name="plots",
    def=function(object,class=NULL){
        standardGeneric("plots")
    }
)

#' @rdname plots-methods
#' @aliases plots
setMethod(
    "plots",
    signature="ANY",
    definition=function(object,class){

        # check class object
        if(!is(object,"vitellines_membranes") & !is(object,"eggshell")){
            stop("object class must be a vitellines_membranes or eggshell.")
        }

        # value type
        if(!is.null(class)){

            # print the graph
            slot(object,"plots")$plot_samples[[class]] +
            facet_wrap(~samples,ncol=2)

        }else{

            if(class(object)=="eggshell"){

                # print the plots
                slot(object,"plots")$plot_samples+
                facet_wrap(~samples,ncol=2,scales="free_x")

            }else{

                # display in 2 columns
                grid.arrange(
                    slot(object,"plots")$plot_samples_class_overlay,
                    slot(object,"plots")$plot_samples_class,
                    ncol=2
                )
            }
        }
    }
)
