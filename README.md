<img src="./inst/extdata/univ.png" align="right"/>
<img src="./inst/extdata/boa.png" align="right"/>
<img src="./inst/extdata/inra.png" align="right"/>

# eggbiomechanic R package

Analysis of egg mechanical properties (eggshell, vitellines membranes).

## Installation

```r
## using install_gitlab
remotes::install_gitlab(
  "aurelien.brionne/eggbiomechanic",
  host = "forgemia.inra.fr",
  build_opts = c("--no-resave-data", "--no-manual")
)

## alternative (from local directory)
    # clone package (from prompt)
    git clone https://forgemia.inra.fr/aurelien.brionne/eggbiomechanic.git

    # build package (from R console) 
    devtools::build("eggbiomechanic")

    # install package (from R console)
    install.packages("eggbiomechanic_1.2.4.tar.gz", repos = NULL, type = "source")
```

## Quick overview

1. Instron &copy; raw data processing: `eggbiomechanic::Instron_eggshell` or `eggbiomechanic::Instron_vitmb`.

2. plot individual Instron &copy; traces: `eggbiomechanic::plots`.

<u>eggshell</u>
    
![](./inst/extdata/instron_eggshell.png)

<u>vitellines membranes</u>
     
![](./inst/extdata/instron_vitmb.png)

![](./inst/extdata/instron2_vitmb.png)

3. Compute eggshell mechanical properties: `eggbiomechanic::biomechanic_eggshell`.

4. Extract results tables: `eggbiomechanic::get_table`.

5. Display table in interactive: `eggbiomechanic::display_table`.

![](./inst/extdata/display_table.png)

6. Fitting Linear Models or Linear Mixed-Effects Models: `eggbiomechanic::var_test`.

![](./inst/extdata/stats.png)
